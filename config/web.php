<?php


$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'street map BACKEND',
    'language'=>'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        
        'mongodb' => require(__DIR__ . '/local.mongo.db.php'),
        
/*        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://street:street@localhost:27017/streetmap',
        ],*/
        


        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'Imagick',  //GD or Imagick
        ],
       

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '5371438',
                    'clientSecret' => 'eyGTo1xfy4DCYOL2Kgqm',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '5f16KY8mXfr-uZPMqnbq_rHroFWffeGo',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableSession' => false
            //'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        #'db' => require(__DIR__ . '/db.php'),
        //Будем использовать свой локальный файл
        'db' => require(__DIR__ . '/local.db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            #'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                /*
                    контроллер у нас ServiceController
                    yii парсить урл так что можно делать запросы на /services (GET - список всех сервисов)
                */
                ['class' => 'yii\rest\UrlRule', 'controller' => 'service'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'userservice'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'settingslist', 'pluralize'=>true],

                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => 'placemark',
                    'tokens' => ['{id}' => '<id:\\w+>']
                ],
            ],

        ],
        'response' => [
            'class' => 'yii\web\Response',
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => 'app\components\CustomJsonResponseFormatter',
                \yii\web\Response::FORMAT_XML => 'app\components\CustomJsonResponseFormatter',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    $config['modules']['gii']['generators']['mongoDbModel']['class'] = 'yii\mongodb\gii\model\Generator';


}

return $config;
