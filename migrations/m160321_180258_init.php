<?php

use yii\db\Migration;
use yii\db\mysql\Schema;
class m160321_180258_init extends Migration
{   

    /*
        Таблицы для авторизации пользователя
        При авторизации пользателя через соц. сети создается пользователь в нашей системе
        И так же создается сервис авторизации пользовтеля "user_services"
        При авторизации через соц. сеть проверяется user_services на наличии кода авторизации соц.сети
        Если всё ок, записываем в сессию user_id.
        Целостность вроде не нарушена, и выглядит всё логично

        Ещё такой баг, если значения заполнены в таблицах, то migrage/down не канает, 
        видимо надо очищать сначала таблицы
    */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // Создаем таблицу пользователей
        $this->createTable('{{%users}}', [
            'user_id' => Schema::TYPE_PK  . ' NOT NULL AUTO_INCREMENT',
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string(255)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        // Создаем таблицу сервисов через который пользователь может авторизовываться
        $this->createTable('{{%user_services}}', [
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'service_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_service_id' => $this->string(255)->notNull(),
        ], $tableOptions);

        // накидываем индексы на столбцы
        $this->createIndex('FK_users_user_id', '{{%users}}', 'user_id');
        $this->createIndex('FK_service_id', '{{%user_services}}', 'service_id');
        $this->createIndex('FK_user_id', '   {{%user_services}}', 'user_id');

        //Накидываем внешние ключи
        $this->addForeignKey(
            "FK_users_user_id", 
            "{{%user_services}}", 
            "user_id", 
            "{{%users}}", 
            'user_id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('{{%services}}', [
            'service_id' => Schema::TYPE_PK  . ' NOT NULL AUTO_INCREMENT',
            'service_name' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createIndex('FK_services_service_id', '{{%services}}', 'service_id');

        $this->addForeignKey(
            "FK_services_service_id", 
            "{{%user_services}}", 
            "service_id", 
            "{{%services}}", 
            'service_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
        $this->dropTable('{{%user_services}}');
        $this->dropTable('{{%services}}');
        

        return true;
    }

}
