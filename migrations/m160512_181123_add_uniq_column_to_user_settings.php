<?php

use yii\db\Migration;

/**
 * Handles adding uniq_column to table `user_settings`.
 */
class m160512_181123_add_uniq_column_to_user_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_settings}}', 'id', 'integer NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user_settings}}', 'id');
    }
}
