<?php

use yii\db\Migration;

class m160520_111812_add_avatar_setting extends Migration
{
    public function up()
    {
        $this->insert('{{%user_settings_types}}',array(
            'setting_type_id' => '1',
            'setting_name'=>'Фото профиля'
        ));
    }

    public function down()
    {
        echo "m160520_111812_add_avatar_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
