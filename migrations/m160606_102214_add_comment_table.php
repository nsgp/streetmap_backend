<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160606_102214_add_comment_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // Создаем таблицу комментов
        $this->createTable('{{%user_comments}}', [
            'id' => Schema::TYPE_PK  . ' NOT NULL AUTO_INCREMENT',
            'module_id' => $this->string(255)->notNull(),
            'visible' => $this->smallInteger()->notNull()->defaultValue(0),
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'answer_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'comment_content' => $this->string()->notNull(),
            'create_at' => $this->integer()->notNull(),
            'update_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_comments}}');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
