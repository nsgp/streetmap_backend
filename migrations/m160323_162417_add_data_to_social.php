<?php

use yii\db\Migration;

class m160323_162417_add_data_to_social extends Migration
{
    public function up()
    {
        $this->insert('{{%services}}', ['service_id'=>1, 'service_name'=>'vkontakte']);
        $this->insert('{{%services}}', ['service_id'=>2, 'service_name'=>'facebook']);
        $this->insert('{{%services}}', ['service_id'=>3, 'service_name'=>'twitter']);
        $this->insert('{{%services}}', ['service_id'=>4, 'service_name'=>'instagram']);
    }

    public function down()
    {
    }
}
