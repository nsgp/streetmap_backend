<?php

use yii\db\Migration;

class m160512_192512_change_settings_length extends Migration
{
    public function up()
    {
        $this->alterColumn('user_settings', 'value', $this->text()->notNull());
    }

    public function down()
    {
        echo "m160512_192512_change_settings_length cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
