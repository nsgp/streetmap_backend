<?php

use yii\db\Migration;

class m160325_182430_bugfix_password_hash extends Migration
{
    public function up()
    {

        $this->alterColumn ( "{{%users}}", 'password_hash', $this->string(255)->notNull()->defaultValue('') );
        $this->alterColumn ( "{{%users}}", 'status', $this->smallInteger()->notNull()->defaultValue(1) );


    }

    public function down()
    {
        echo "m160325_182430_bugfix_password_hash cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
