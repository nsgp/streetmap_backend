<?php

use yii\db\Migration;

class m160513_050516_add_default_types extends Migration
{
    public function up()
    {

        $this->insert('{{%user_settings_types}}',array(
            'setting_type_id' => '2',
            'setting_name'=>'Статус'
        ));

        $this->insert('{{%user_settings_types}}',array(
            'setting_type_id' => '2',
            'setting_name'=>'Любимая цитата'
        ));

        $this->insert('{{%user_settings_types}}',array(
            'setting_type_id' => '3',
            'setting_name'=>'Несколько слов о себе'
        ));

    }

    public function down()
    {
        echo "m160513_050516_add_default_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
