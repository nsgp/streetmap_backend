<?php

use yii\db\Migration;

class m160325_181304_change_email_allow_null extends Migration
{
    public function up()
    {
        $this->alterColumn ( "{{%users}}", 'email', $this->string(255) );
    }

    public function down()
    {
        echo "m160325_181304_change_email_allow_null cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
