<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160510_164958_tables_for_user_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings_types}}', [
            'setting_type_id' => Schema::TYPE_PK  . ' NOT NULL AUTO_INCREMENT',
            'setting_type' => $this->string(100)->notNull(),
        ], $tableOptions);

        $this->createTable('{{%user_settings_types}}', [
            'user_type_id' => Schema::TYPE_PK  . ' NOT NULL AUTO_INCREMENT',
            'setting_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'setting_name' => $this->string(255)->notNull(),
        ], $tableOptions);

        // Создаем таблицу пользователей
        $this->createTable('{{%user_settings}}', [
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'setting_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('FK_user_settings_types_setting_type_id', '{{%user_settings_types}}', 'setting_type_id');
        $this->createIndex('FK_user_settings_user_id', '{{%user_settings}}', 'user_id');
        $this->createIndex('FK_user_settings_setting_type_id', '{{%user_settings}}', 'setting_type_id');

        $this->addForeignKey(
            "FK_user_settings_types_setting_type_id",
            "{{%user_settings_types}}",
            "setting_type_id",
            "{{%settings_types}}",
            'setting_type_id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            "FK_user_settings_setting_type_id",
            "{{%user_settings}}",
            "setting_type_id",
            "{{%user_settings_types}}",
            'user_type_id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            "FK_user_settings_user_id",
            "{{%user_settings}}",
            "user_id",
            "{{%users}}",
            'user_id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert('{{%settings_types}}',array(
            'setting_type' => 'FILE'
        ));

        $this->insert('{{%settings_types}}',array(
            'setting_type' => 'TEXTFIELD'
        ));

        $this->insert('{{%settings_types}}',array(
            'setting_type' => 'TEXTAREA'
        ));

    }

    public function down()
    {
        $this->dropTable('{{%user_settings}}');
        $this->dropTable('{{%user_settings_types}}');
        $this->dropTable('{{%settings_types}}');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
