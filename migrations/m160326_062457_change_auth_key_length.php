<?php

use yii\db\Migration;

class m160326_062457_change_auth_key_length extends Migration
{
    public function up()
    {
        $this->alterColumn ( "{{%users}}", 'auth_key', $this->string(1024)->notNull() );
    }

    public function down()
    {
        echo "m160326_062457_change_auth_key_length cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
