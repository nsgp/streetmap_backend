Yii2 StreetMap backend
============================

# api methods

[METHODS.md](<./METHODS.md>)

# how to use

http://streetmap.backend/auth/login - авторизация. На выходе имеем token

Необходимые поля:

 - LoginForm[email]
 - LoginForm[password]

http://streetmap.backend/auth/registration

Необходимые поля

 - RegistrationForm[username]
 - RegistrationForm[email]
 - RegistrationForm[password]

FB: https://www.facebook.com/dialog/oauth?%20client_id=523491474478738&redirect_uri=http://streetmap.backend/userservice/fb

VK: https://oauth.vk.com/authorize?client_id=5371438&display=page&redirect_uri=http://streetmap.backend/userservice/vk&callback&scope=friends&response_type=code&v=5.50

При успешной авторизации или регистрации имеем:
```sh
{
    "status":true,
    "token":"<TOKEN>"
}
```
C помощью даного токена пользователь получает доступ к сайту.

# install

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      migrations/         contains db migrations
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.




### Database






CONFIGURATION
-------------

first:
```sh
sudo composer global require "fxp/composer-asset-plugin:~1.1.1"
```
```sh
$ git clone git@bitbucket.org:nsgp/streetmap_backend.git
$ cd street_backend
$ composer install
$ cd config
$ cp db.php local.db.php
$ nano local.db.php #change login, password, db
$ cd ..
$ php yii migrate
```
then you need to configure the HTTP server

>nginx config file example:
```nginx



server {
    listen 808;

    # NOTE: Replace with your path here
    root /YOU_PATH/street_backend/web;
    index index.php;

    # NOTE: Replace with your hostname
    server_name YOU_SERVER_NAME;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        #Обязательно для авторизации
        if ($request_method = 'OPTIONS') {
           add_header 'Access-Control-Allow-Origin' '*';
           add_header 'Access-Control-Allow-Headers' 'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since';
           add_header 'Access-Control-Allow-Credentials' 'true';
           add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
           add_header 'Content-Type' 'text/plain charset=UTF-8';
           add_header 'Content-Length' 0;
           return 200;
        }

        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }

    location ~ /\.ht {
       deny all;
    }
}
```

nginx fast start:

```sh
$ cd /etc/nginx/sites-available 
$ nano subl streetmap_backend #config file
$ cd ../sites-enabled/
$ ln -s ../sites-available/streetmap_backend 
$ service nginx restart
 * Restarting nginx nginx 
```

check this YOU_SERVER_NAME:808, backend is work, good!

![Image of Yaktocat](https://pp.vk.me/c627521/v627521145/48839/0siXQYbCQXg.jpg)

### Social auth settings

config/params.php

```
<?php

return [
    'adminEmail' => 'admin@example.com',
    'social_config'=>[
        1=>[
            'client_id' => '5371438',
            'client_secret' => 'eyGTo1xfy4DCYOL2Kgqm',
            'auth_server'=>'https://oauth.vk.com/access_token',
            'accept_redirect_url'=>'http://streetmap.backend/userservice/vk'
        ],
        2=>[
            'client_id' => '523491474478738',
            'client_secret' => '2c9999efa488489c9a0612ff6c145fef',
            'auth_server'=>'https://graph.facebook.com/v2.3/oauth/access_token',
            'accept_redirect_url'=>'http://streetmap.backend/userservice/fb'
        ]
    ]
];

```

USAGE

POST  /points

REQUEST EXAMPLE 
```sh
INPUT
lat: 54.97574182583224
lng: 82.899227142334}

OUTPUT
{
  "hash": "cec936bc5",
  "exist": false,
  "coordinates": {
    "lat": "54.97574182583224",
    "lng": "82.89304733276369"
  },
  "city_id": 1,
  "centroid": "POINT(82.897663 54.972534)",
  "attributes": {
    "small_description": "Новосибирск, Кировский"
  }
}
```