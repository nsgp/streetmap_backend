<?php

namespace app\tests\codeception\unit\fixtures;

use yii\mongodb\ActiveFixture;

class PlacemarksFixture extends ActiveFixture
{
	public $collectionName = 'points_1';
    #public $modelClass = 'app\models\mongo\Placemarks';
    #public $depends = ['app\tests\fixtures\UserFixture'];
}