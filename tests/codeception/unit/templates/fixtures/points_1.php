<?php

$faker = Faker\Factory::create();

$region_url = 'http://catalog.api.2gis.ru/2.0/region/list?key=ruczoy1743&page_size=1000&fields=items.default_pos&locale_filter=ru_RU';

$ch = curl_init($region_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$contents = json_decode(curl_exec($ch));

$end = (int)rand(0,200);

foreach ($contents->result->items as $key => $cnt) {

	$ctn_id = (int)$cnt->id;

	$lon = $cnt->default_pos->lon;
	$lat = $cnt->default_pos->lat;

	for ($i=0; $i < (int)rand(70,100); $i++) { 
		$result[] = [
			'coordinates'=>['lat'=>$faker->latitude($lat-0.5555, $lat+0.5555), 'lng'=>$faker->latitude($lon-0.5222, $lon+0.5222)],
		    'user_id' => 1,
		    'city_id' => (int)$ctn_id,
		    'centroid' => false,
			'attributes' => [
				'small_description'=>$faker->streetName,
				'description'=>$faker->text($maxNbChars = 1500),
				'user_title'=>$faker->text($maxNbChars = 200),
				'painted_over'=>rand(0,1),
				'free_watch'=>rand(0,1),
				'point_type'=>rand(0,4)
			]
		];
	}

}
return $result;


/*return [
	'coordinates'=>['lat'=>$faker->latitude(55.1413, 54.8439), 'lng'=>$faker->latitude(82.7743, 83.1084)],
    'user_id' => 1,
    'city_id' => 1,
    'centroid' => false,
	'attributes' => [
		'small_description'=>$faker->streetName,
		'description'=>$faker->text($maxNbChars = 1500),
		'user_title'=>$faker->text($maxNbChars = 200)
	]
   // 'intro' => $faker->sentence(7, true),  // generate a sentence with 7 words
];

*/
