# Использование
Закрытые методы требуют ключ API


### GET /region/all
Открытый метод /region/all - возвращает регионы

OUTPUT:
```
{
  "total": 92,
  "items": [
    {
      "name": "Абакан",
      "id": "69",
      "type": "region",
      "default_pos": {
        "lon": 91.442472,
        "lat": 53.720644,
        "zoom": 11
      },
      "placemarks_count":0
    },
...
    {
      "name": "Ярославль",
      "id": "28",
      "type": "region",
      "default_pos": {
        "lon": 39.897301,
        "lat": 57.625077,
        "zoom": 11
      },
      "placemarks_count":0
    }
  ]
}
```


### GET /placemarks/<PLACEMARK_ID>
открытый метод /placemarks/<PLACEMARK_ID> - возвращает конкретную точку


### GET /placemarks возвращает точки
GET  /placemarks

 необязательные параметры

 - fields [coordinates, hash, city_id, centroid, attributes]
 - expand [img]
 - city [{INT} Делает выборку по городам]

Примеры запросов
 - http://streetmap.backend:808/placemarks?city_id=1&fields=coordinates
 - http://streetmap.backend:808/placemarks?city_id=1&expand=img&fields=coordinates
 - http://streetmap.backend:808/placemarks&fields=coordinates
 - http://streetmap.backend:808/placemarks

/placemarks
 
OUTPUT:
```sh
[
  {
    "coordinates": {
      "lat": "55.04940833333",
      "lng": "82.915130555556"
    },
    "hash": "4ea51abb8",
    "city_id": 1,
    "centroid": "POINT(82.926145 55.034023)",
    "attributes": {
      "small_description": "Новосибирск, Центральный"
    }
  },
  {
    "coordinates": {
      "lat": "55.04940833333",
      "lng": "82.915110555556"
    },
    "hash": "b3642ddcc",
    "city_id": 1,
    "centroid": "POINT(82.910596 55.065937)",
    "attributes": {
      "small_description": "Новосибирск, Красный проспект"
    }
  }
]
```


### POST /placemarks
Закрытый метод /placemarks - Добавляет координату.

Обязательные параметры:
 - lat
 - lng
 - user_title

Не обязательные параметры 
 - description - описание
 - images[] - изображения

OUTPUT:
```php
{
  "_id": {
    "$id": "5704ba8df45be0a5098b4572"
  },
  "attributes": {
    "small_description": "Новосибирск, Красный проспект"
  },
  "centroid": "POINT(82.910596 55.065937)",
  "city_id": 1,
  "coordinates": {
    "lat": "55.04940833333",
    "lng": "82.915110555556"
  },
}
```

МЕТОД В РАЗРАБОТКЕ
### POST /placemarks/image

Закрытый метод. Добавляет фотографии к точке 

http://streetmap.backend:808/placemarks/image?access-token=<TOKEN>
```
POST DATA: 
->input
hash: <placemarks_id> (Например 5704cc8bf45be076068b4568)
images[]: 5704cb1038325 (photo_id)

->output
{
  "status": true
}

OR

example:
{
  "status": false,
  "errors": [
    "image not found"
  ]
}
```

### POST /upload/image

http://streetmap.backend:808/upload/image?access-token=<>

Загружает картинку, и геокодирует координаты, если такие имеются в фотографии 

POST: 
data: <file>

```
OUTPUT:
{
  "status": true,
  "images": {
    "o": "uploads/9dcb5966df0b2fb0c04304911db26264/299ef79d51.jpg",
    "s": "uploads/9dcb5966df0b2fb0c04304911db26264/s299ef79d51.jpg",
    "m": "uploads/9dcb5966df0b2fb0c04304911db26264/m299ef79d51.jpg"
  },
  "coordinates": {
    "lat": 55.049408333333,
    "lon": 82.915130555556
  },
  "user_id": 39,
  "id": "57299ef8f45be069068b456f"
}
```

### POST /settingslist/image

Закрытый метод /settingslist/image - добавляет точки и обрезает её по заданным параметрам

Необходимые поля

 - file[USER_CONFIG_TYPE_ID]
 - x0 - Ширина
 - y0 - Высота
 - x1 - Смещение по X
 - y1 - Смещение по Y


OUTPUT:
```
{
  "status": true
}
```
http://streetmap.backend:808/settingslist/image?access-token=fnj7ipLJRsE4Mp46jHAsAJlW6o0FWLvsh8q7LWIwtP9Yq4yZFzHGcXVxp2OiyHiuf0xr39_6TPNY56Hql6beH
