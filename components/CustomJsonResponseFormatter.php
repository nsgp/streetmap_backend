<?php

namespace app\components;

use Yii;
use yii\helpers\Json;
use yii\web\JsonResponseFormatter;

class CustomJsonResponseFormatter extends JsonResponseFormatter
{
    public $encodeOptions = JSON_UNESCAPED_UNICODE;

    /**
     * @inheritdoc
     */
    protected function formatJson($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        $response->content = Json::encode($response->data, $this->encodeOptions);
    }
}
