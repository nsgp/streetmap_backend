<?php

/**
* Parse coordinates from exif data
* use: 
* $coods = new exifCoordinates(exif_read_data('test.jpg'));
* $coods = $coods->getData();
* return data: [54.955269444444, 82.938666666667]
*/
namespace app\components;

class exifCoordinates
{
	public $exif_data = false;

	function __construct( $exif = false )
	{	
		if($exif==false){
			return false;
		} else {
			return $this->exif_data = $exif;
		}
	}

	public function getData(){
		return $this->exif_to_lat_lon($this->exif_data);
	}
	//Pass in GPS.GPSLatitude or GPS.GPSLongitude or something in that format
	public function parse_fract( $f )
	{
		$nd = explode( '/', $f );
		return $nd[0] ? ($nd[0]/$nd[1]) : 0;
	}

	public function parse_lat_lon( $arr )
	{
		$v=0;
		$v += $this->parse_fract( $arr[0] );
		$v += $this->parse_fract( $arr[1] )/60;
		$v += $this->parse_fract( $arr[2] )/3600;
		return $v;
	}

	public function exif_to_lat_lon( $exif ) {

		if($exif==false){
			return false;
		}
		
		$exif = array_intersect_key( $exif, array_flip( array('GPSLatitudeRef', 'GPSLatitude', 'GPSLongitudeRef', 'GPSLongitude') ) );

		if ( count($exif)!=4 )
			return '';
		if ( !in_array($exif['GPSLatitudeRef'], array('S', 'N') ) )
			return 'GPSLatitudeRef not S or N';
		if ( !in_array($exif['GPSLongitudeRef'], array('W', 'E') ) )
			return 'GPSLongitudeRef not W or E';
		if (!is_array($exif['GPSLatitude']) or !is_array($exif['GPSLongitude']) )
			return 'GPSLatitude and GPSLongitude are not arrays';
			
		$lat = $this->parse_lat_lon( $exif['GPSLatitude'] );
		if ( $exif['GPSLatitudeRef']=='S' )
			$lat = -$lat;
		$lon = $this->parse_lat_lon( $exif['GPSLongitude'] );
		if ( $exif['GPSLongitudeRef']=='W' )
			$lon = -$lon;
		return ['lat'=>$lat, 'lon'=>$lon];
	}
}