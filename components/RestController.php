<?php

namespace app\components;

use yii\filters\Cors;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class RestController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => CompositeAuth::className(),
//            'authMethods' => [
//                [
//                    'class' => QueryParamAuth::className(),
//                    'tokenParam' => 'session_token'
//                ],
//                [
//                    'class' => CookieParamAuth::className(),
//                    'tokenParam' => 'session_token'
//                ]
//            ]
//        ];
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => 'yii\filters\Cors',
        ];
        return $behaviors;
    }
}
