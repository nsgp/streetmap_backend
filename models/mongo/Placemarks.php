<?php

namespace app\models\mongo;

use Yii;

/**
 * This is the model class for collection "Points".
 *
 * @property \MongoId|string $_id
 */
class Placemarks extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['streetmap', 'points_1'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'user_id',
            'user_title',
            'coordinates',
            'city_id',
            'centroid',
            'description',
            'attributes',
            'images',
            'painted_over',
            'free_watch',
            'point_type'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_title'], 'required'], 
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'user_title' => Yii::t('app', 'Заголовок'),
        ];
    }

    public function getImg() {

        if(empty($this->images)){
            return false;
        } else {

            $images = UserImages::find()->select(['photo_id', 'user_id', 'coordinates', 'files'])->where(['in', '_id', $this->images])->all(); 

            if(!empty($images)){
                return $images;
            } else {
                return false;
            }
        
        }


        //$me = $query->from('points_' . $geoData->project_id)->where(['hash' => $hash]);

        //return $me;
    }

    public function Fields()
    {
        return [
            '_id',
            'user_id',
            'user_title',
            'coordinates',
            'city_id',
            'centroid',
            'description',
            'attributes',
            'painted_over',
            'free_watch',
            'point_type'
        ];
    }

    public function extraFields()
    {
        return [
            'img',
        ];
    }
}
