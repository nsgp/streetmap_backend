<?php

namespace app\models\mongo;

use Yii;
use app\components\exifCoordinates;
use yii\helpers\BaseFileHelper;

/**
 * This is the model class for collection "user_images_2".
 *
 * @property \MongoId|string $_id
 */


class UserImages extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {   
        return ['streetmap', 'user_images_1'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'url',
            'photo_id',
            'description',
            'files',
            'coordinates',
            'user_id'
        ];
    }

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg, jpeg', 'maxSize' => 1024 * 1024 * 10],
            #[['point_id'], 'integer'],
            [['url'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 65535]
        ];
    }

    /**
     * @inheritdoc
     */
    public $imageFile;
    public $uploadFolder = 'uploads';

    public function imageModify($w, $h, $image, $path, $preview = false ) {

        if($preview){
            $p=$w / $h;
            $pImage=$image->width / $image->height;
            // если $pImage > $p значит высота изображения в пропорции будет меньше нужной, 
            // масштабируем по высоте, иначе по ширине 
            $master=($pImage > $p) ? \yii\image\drivers\Image::HEIGHT : \yii\image\drivers\Image::WIDTH;
            $image->resize($w, $h, $master)
                ->crop($w, $h)
                ->save($path);   
        } else {
            $image->resize($w, $h, 0x07)->background('#df691a')->save($path);    
        }
    }

    public function upload()
    {   

        if(!is_dir($this->uploadFolder)){
            BaseFileHelper::createDirectory($this->uploadFolder);
        }
        $dir_prefix = md5(substr(time(), 0,-4).'0000');
        //$dir_prefix = md5(rand(0,200).'0000');

        if(!is_dir($this->uploadFolder . '/' . $dir_prefix)) {
            BaseFileHelper::createDirectory($this->uploadFolder . '/' . $dir_prefix);
        }   

        if ($this->validate()) {
            #$image_path = $this->uploadFolder . '/'. $dir_prefix .'/'. md5($this->imageFile->baseName . time()) . '.' . $this->imageFile->extension;
            $file_name =  substr(uniqid(rand(),true),12,10) . '.' . $this->imageFile->extension;
            $image_path = $this->uploadFolder . '/'. $dir_prefix . '/';
            $images = [];
            $images['o'] = $image_path . $file_name;
            if($this->imageFile->saveAs($images['o'])){
                
                $this->user_id  = Yii::$app->user->identity->user_id;

                $coods = new exifCoordinates(exif_read_data($images['o']));
                $coordinates = $coods->getData();
                
                if(!empty($coordinates)){
                    $this->coordinates = $coordinates;
                } else {
                    $this->coordinates = false;
                }

                $images['s'] = $image_path . 's'. $file_name;
                $images['m'] = $image_path . 'm'. $file_name;

                $image=Yii::$app->image->load($images['o']);              
                $this->imageModify(50, 50, $image, $images['s'], true);

                $image = Yii::$app->image->load($images['o']);
                $this->imageModify(150, 150, $image, $images['m'], true);
                return ['status'=>true, 'content'=>['images'=>$images, 'coordinates'=>$this->coordinates, 'user_id'=>$this->user_id]];  
            } else {
                return ['status'=>false, 'error'=>'unknown'];  
            }
        } else {
            return ['status'=>false, 'error'=>$this->getErrors()];  
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
        ];
    }
}
