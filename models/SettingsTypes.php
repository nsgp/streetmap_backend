<?php

namespace app\models;

use Yii;
use yii\validators\FileValidator;

/**
 * This is the model class for table "{{%settings_types}}".
 *
 * @property integer $setting_type_id
 * @property string $setting_type
 *
 * @property UserSettingsTypes[] $userSettingsTypes
 */
class SettingsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings_types}}';
    }



    public $TEXTFIELD;
    public $TEXTAREA;
    public $FILE;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_type'], 'required'],
            [['setting_type'], 'string', 'max' => 100],
            [['TEXTFIELD', 'TEXTAREA', 'FILE'], 'safe'],
            
            [
                ['FILE'], 
                'image',
                'extensions' => 'png,jpg', 
                //'maxSize' => 1024*1024*50,
                'minWidth' => 100, 'maxWidth' => 4000,
                'minHeight' => 100, 'maxHeight' => 4000,

            ],

            ['TEXTFIELD', 'string', 'max' => 255, 'tooLong'=>'Максимальное колличество символов - {max})'],
            ['TEXTAREA', 'string', 'max' => 4096, 'tooLong'=>'Максимальное колличество символов - {max})'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_type_id' => Yii::t('app', 'Setting Type ID'),
            'setting_type' => Yii::t('app', 'Setting Type'),
            'TEXTFIELD' => Yii::t('app', 'Текстовое поле'),
            'TEXTAREA' => Yii::t('app', 'Текстовое поле'),
            'FILE' => Yii::t('app', 'Текстовое поле'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettingsTypes()
    {
        return $this->hasMany(UserSettingsTypes::className(), ['setting_type_id' => 'setting_type_id'])->inverseOf('settingType');
    }
}
