<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_settings}}".
 *
 * @property integer $user_id
 * @property integer $setting_type_id
 * @property string $value
 *
 * @property UserSettingsTypes $settingType
 * @property Users $user
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'setting_type_id', 'value'], 'required'],
            [['user_id', 'setting_type_id'], 'integer'],
            [['setting_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserSettingsTypes::className(), 'targetAttribute' => ['setting_type_id' => 'user_type_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'setting_type_id' => Yii::t('app', 'ID конфигурации пользователя'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingType()
    {
        return $this->hasOne(UserSettingsTypes::className(), ['user_type_id' => 'setting_type_id'])->inverseOf('userSettings');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id'])->inverseOf('userSettings');
    }
}
