<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use app\models\mongo\UserImages;

/* Модель регистрации */
use frontend\models\SignupForm;

/**
 * This is the model class for table "users".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UserServices[] $userServices
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['register'] = [
            'email',
            'password_hash',
            'auth_key',
        ];
        //$scenarios['edit'] = ['user_id','question'];

        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required',  'on' => self::SCENARIO_REGISTER],
            [['username', 'auth_key'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /*    public function getImages() {
        $me = UserImages::find()->all();
        return $me;
    }*/

    public function newUserFromService($username, $auth_key, $status){
        $this->username = $username;
        $this->auth_key = $auth_key;
        $this->status = $status;
        if($this->save()){
            return $this->user_id;
        } else {
            return false;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */


    public static function findIdentityByAccessToken($token, $type = null)
    {   
        //Проверяем токен
        $user_model = Users::find()->where(['auth_key'=>$token])->one();
        if(!empty($user_model)){
            //$user_model['accessToken'] === $token;
            return $user_model;
        }
        return null;
    }

    public static function findIdentity($id)
    {

        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }


    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function getUserServices()
    {
        return $this->hasMany(UserServices::className(), ['user_id' => 'user_id'])->inverseOf('user');
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    //set sec password
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    //Token gen
    public function generateAuthKey( $length = 85)
    {
        $this->auth_key = Yii::$app->security->generateRandomString($length);
    }

    public function extraFields()
    {

        return [
            'userServices',
            'images',
            'settings'
        ];
    }

    public function getSettings()
    {   
        $elements = $this->hasMany(UserSettings::className(), ['user_id' => 'user_id'])->all();
        $values = [];
        foreach($elements as $key => $elem) {
            $values[$elem->setting_type_id] = [
                'id'=>$elem->id,
                'user_id'=>$elem->user_id,
                'setting_type_id'=>$elem->setting_type_id,
                'value'=> !empty($elem->value) ? unserialize($elem->value) : false,
                'name'=>$elem->settingType->setting_name
            ];
        }
        return $values;
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => 1]);
    }

    public function fields()
    {
        $fields = parent::fields();
        //Убиваем ненужные поля
        unset(
            $fields['auth_key'],
            $fields['password_hash'],
            $fields['password_reset_token'],
            $fields['updated_at'],
            $fields['status'],
            $fields['created_at']
        );

        return $fields;
    }
}
