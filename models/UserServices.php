<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_services".
 *
 * @property integer $user_id
 * @property integer $service_id
 * @property string $user_service_id
 *
 * @property Services $service
 * @property Users $user
 */
class UserServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'service_id', 'user_service_id'], 'required'],
            [['user_id', 'service_id'], 'integer'],
            [['user_service_id'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'service_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'service_id' => Yii::t('app', 'Service ID'),
            'user_service_id' => Yii::t('app', 'User Service ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['service_id' => 'service_id'])->inverseOf('userServices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id'])->inverseOf('userServices');
    }

    public function addUserService($service_id, $user_service_id, $user_id){
        $this->service_id = $service_id;
        $this->user_service_id = (string)$user_service_id;
        $this->user_id = $user_id;

        if(!$this->save()){
            return false;
        } else {
            return true;
        }
    }
}
