<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_settings_types}}".
 *
 * @property integer $user_type_id
 * @property integer $setting_type_id
 * @property string $setting_name
 *
 * @property UserSettings[] $userSettings
 * @property SettingsTypes $settingType
 */
class UserSettingsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_type_id', 'setting_name'], 'required'],
            [['setting_type_id'], 'integer'],
            [['setting_name'], 'string', 'max' => 255],
            [['setting_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SettingsTypes::className(), 'targetAttribute' => ['setting_type_id' => 'setting_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_type_id' => Yii::t('app', 'User Type ID'),
            'setting_type_id' => Yii::t('app', 'ID конфигурации пользователя'),
            'setting_name' => Yii::t('app', 'Setting Name'),
        ];
    }

    public function Fields()
    {
        return [
            'user_type_id',
            'setting_name',
            'settingType',
            'settings'
        ];
    }

    public function getSettings() {
        $elem = UserSettings::find()->select(['value'])->where(['user_id'=>Yii::$app->user->identity->user_id, 'setting_type_id'=>$this->user_type_id])->one();
        if(!empty($elem)) {
            $elem->value = unserialize($elem->value);
            return $elem;
        } else {
            return false;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSettings::className(), ['setting_type_id' => 'user_type_id'])->inverseOf('settingType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingType()
    {
        return $this->hasOne(SettingsTypes::className(), ['setting_type_id' => 'setting_type_id'])->inverseOf('userSettingsTypes');
    }
}
