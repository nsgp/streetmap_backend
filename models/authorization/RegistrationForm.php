<?php
namespace app\models\authorization;


use app\models\Users;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class RegistrationForm extends Model
{
    public $email;
    public $username;
    public $password;
    public $passwordverify;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username'], 'filter', 'filter' => 'trim'],
            [['email', 'username'], 'required'],
            ['email', 'unique', 'targetClass' => '\app\models\Users', 'message' => 'e-mail уже занят'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            [['passwordverify'], 'verifyPassword'],
            [['password', 'passwordverify'], 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Имя пользователя'),
            'password' => Yii::t('app', 'Пароль'),
            'passwordverify' => Yii::t('app', 'Подтверждение пароля'),
            'email' => Yii::t('app', 'e-mail'),
        ];
    }

    public function verifyPassword($attribute, $params)
    {   
        if($this->$attribute != $this->password) {
            return $this->addError($attribute, 'Пароли не совпадают');    
        }
    }

    /**
     * Signs user up.
     *
     * @return gitoken or errors
     */
    public function signup()
    {
        if (!$this->validate()) {
            return [
                'status'=>false,
                'errors'=>$this->getErrors()
            ];
        }

        $user = new Users();
        $user->email = $this->email;
        $user->username = $this->username;
        $user->status = 1;
        $user->setPassword($this->password);
        $user->generateAuthKey(85);

        if($user->save()) {
            return [
                'status'=>true,
                'token'=>$user->auth_key
            ];
        } else {
            return [
                'status'=>false,
                'errors'=>$user->getErrors()
            ];
        }
    }
}
