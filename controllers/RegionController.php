<?php
/**
 * Created by PhpStorm.
 * User: sega
 * Date: 23.03.16
 * Time: 23:26
 */

namespace app\controllers;


use app\components\RestController;

use yii\web\ForbiddenHttpException;
use app\models\mongo\Placemarks;
use yii\filters\AccessControl;

use yii\helpers\Url;
use Yii;

class RegionController extends RestController
{
	public $modelClass = 'app\models\mongo\Placemarks';

	public function behaviors()
	{

		$behaviors = parent::behaviors();

		$behaviors['access'] = [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'all'],
						'verbs' => ['GET'],
						'roles' => ['?'],
					],
					[
						'allow' => false,
						'matchCallback'=>function() {
							throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
						}
					],
				],
			];

		return $behaviors;
	}

	protected function verbs()
	{
		return [
			'all' => ['GET']
		];
	}

	public function actions()
	{
		$actions = parent::actions();

		unset($actions['index']);
		unset($actions['all']);
		return $actions;
	}

	public $base_url = "http://catalog.api.2gis.ru/geo/search";
	public $api_key = 'ruczoy1743';

	public function actionIndex() {
		return $this->actionAll();
	}

	public function actionAll() {
		$region_url = 'http://catalog.api.2gis.ru/2.0/region/list?key='.$this->api_key.'&page_size=1000&fields=items.default_pos&locale_filter=ru_RU';

		$ch = curl_init($region_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$contents = json_decode(curl_exec($ch));

		#//Преобразовываем объект в массив для далнейшего поиска по ключам
		for ($i=0; $i < count($contents->result->items); $i++) {
			$city_array[$contents->result->items[$i]->id] = get_object_vars($contents->result->items[$i]);
			$city_array[$contents->result->items[$i]->id]['placemarks_count'] = 0;
		}

		//Поиск городов по городам которые вернул 2gis
		$mongoElements = Placemarks::find()->asArray()->
		select(['city_id'])->
		where(['in', 'city_id', array_keys($city_array)])->all();

		//Смотрим считаем колличество элементов у нас в базе
		for ($i=0; $i < count($mongoElements); $i++) {
			if(array_key_exists($mongoElements[$i]['city_id'], $city_array)){
				$city_array[$mongoElements[$i]['city_id']]['placemarks_count']++;
			}
		}

		return ['total'=>$contents->result->total ,'items'=>array_values($city_array)];
	}
}