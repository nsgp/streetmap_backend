<?php
/**
 * Created by PhpStorm.
 * User: sega
 * Date: 23.03.16
 * Time: 23:26
 */

namespace app\controllers;


use app\components\RestController;

use yii\web\UploadedFile;

use app\models\UserSettings;
use app\models\UserSettingsTypes;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use Yii;

use yii\helpers\BaseFileHelper;

class SettingslistController extends RestController
{
    public $modelClass = 'app\models\UserSettingsTypes';

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'verbs' => ['GET'],
                        'roles' => ['@'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['create', 'image'],
                        'verbs' => ['POST'],
                        'roles' => ['@'],
                    ],
 
                    [
                        'allow' => false,
                        'matchCallback'=>function() {
                            throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                        }
                    ],
                ],
            ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }

    private function modelProcess( $user_setting_id ) {

    	$user_setting_model = UserSettings::findOne([
    	    'user_id'=>Yii::$app->user->identity->user_id,
    	    'setting_type_id'=>$user_setting_id
    	]);

    	$settingType = null;

    	if(empty($user_setting_model)) {
    	    $user_setting_model = new UserSettings;
    	    $user_setting_model->user_id = Yii::$app->user->identity->user_id;
    	    $user_setting_model->setting_type_id = $user_setting_id;
    	    $user_setting_model->value = false;
    	    if(!$user_setting_model->save()) {
    	        return ['result'=>false, 'errors'=>['user_setting_id'=>$user_setting_id, 'errors'=>$user_setting_model->getErrors()]];
    	    } else {
    	        return ['result'=>true, 'model'=>$user_setting_model];
    	    }
    	} else {
    	    return ['result'=>true, 'model'=>$user_setting_model];
    	}

    }

    public function actionImage() {

    	if(isset($_FILES['file'])) {
        	$settings_model = new UserSettingsTypes;
    		$errors = [];

    		foreach($_FILES['file']['name'] as $user_setting_id => $fileFromFrontEnd) {
    			static $i = 0;

    			$settings_model->setting_type_id = $user_setting_id;

    			if(!$settings_model->validate(['user_type_id'])) {
    			    $errors[] = ['user_setting_id'=>$user_setting_id, 'errors'=>$settings_model->getErrors()];
    			}

    			$user_setting_model = $this->modelProcess($user_setting_id);
    			if($user_setting_model['result']) {
    				$user_setting_model = $user_setting_model['model'];
    			} else {
    				$errors[] = $user_setting_model['errors'];
    				continue;
    			}
    			$settingType = $user_setting_model->settingType->settingType;
    			$type = $settingType->setting_type;

    			//return $settingType->setting_type_id;

            	$settingType->$type = UploadedFile::getInstancesByName('file')[$i];

            	if(!$settingType->validate([$type])) {
            	    $errors[] = ['user_setting_id'=>$user_setting_id, 'errors'=>$settingType->getErrors()[$type]];
            	} else {

            		$image_place = 'uploads/userimages/';


            		if(!is_dir($image_place)) {
            		    BaseFileHelper::createDirectory($image_place);
            		}



            		$file_name =  $image_place . substr(uniqid(rand(),true),12,10) . '.' . $settingType->$type->extension;
            	       
                    $x0 = (string)Yii::$app->request->post('x0', false);
                    $y0 = (string)Yii::$app->request->post('y0', false);
                    $x1 = (string)Yii::$app->request->post('x1', false);
                    $y1 = (string)Yii::$app->request->post('y1', false);

                   

                    $postForResize = ((!strlen($x0) || $x0<0) || (!strlen($y0) || $y0<0) || (!strlen($x1) || $x1<0) || (!strlen($y1) || $y1<0));
                   # $coordinatesFormat = ($x0 >= $x1) || ($y0 >= $y1); 
                    if($x0!=$y0) {
                        $errors[] = 'error: H must be == Y';    
                    }

                    if($postForResize ) {
                        $errors[] = 'wrong coordinates: coordinates POST error';  

                    }
                    #    $errors[] = ['status'=>false, 'user_setting_id'=>$user_setting_id, 'errors'=>['wrong coordinates']];

                    if(!empty($errors)){
                        return ['status'=>false, 'user_setting_id'=>$user_setting_id, 'errors'=>$errors];
                    }

                    if($settingType->$type->saveAs($file_name)){

                        $image = Yii::$app->image->load($file_name);
                        
                        if(($x1 > $image->width || $y1 > $image->height) || ($x0 > $image->width || $y0 > $image->height)) {
                            $errors[] = 'wrong coordinates: image coordinates < post coordinates';
                             return ['status'=>false, 'user_setting_id'=>$user_setting_id, 'errors'=>$errors];
                        } else {

                            $cropped_name_1 = substr(uniqid(rand(),true),12,10) . '.' . $settingType->$type->extension; 
                            $cropped_name_2 = substr(uniqid(rand(),true),12,10) . '.' . $settingType->$type->extension; 
                            $cropped_name_3 = substr(uniqid(rand(),true),12,10) . '.' . $settingType->$type->extension; 
                            #$image->crop($x0, $y0, $x1, $y1)->save('test' . $settingType->$type->extension);
                            $image->crop($x0, $y0, $x1, $y1)->save($image_place . $cropped_name_1);

                            $image->resize(50, 50, \yii\image\drivers\Image::NONE)->save($image_place . $cropped_name_2);
                            $image->resize(150, 150, \yii\image\drivers\Image::NONE)->save($image_place . $cropped_name_3);
                            #->save('test2.' . $settingType->$type->extension);
                            #$image->crop($x0, $y0, $x1, $y1)->save('test2.' . $settingType->$type->extension);
                        }
                        //return $image;
            	    }

            	    if(empty($setting)) {
            	        $setting = "";
            	    }

            	    $files = [
                        'original'=>$image_place . $file_name, 
                        'c_o'=>$image_place . $cropped_name_1, 
                        's'=>$image_place . $cropped_name_2, 
                        'm'=>$image_place . $cropped_name_3
                    ];

            	    $user_setting_model->value = serialize($files);
            	    $user_setting_model->save();
            	}

/*            	return $settingType->getErrors();

    			$i++;

    			UploadedFile::getInstancesByName('image')[0];

    			return $type;	*/

    		}
    		
    		if(empty($errors)) {
    		    return ['status'=>true];
    		} else {
    		    return ['status'=>true, 'errors'=>$errors];
    		}

    	} else {
    		return ['status'=>false, 'errors'=>['files not found']];
    	}
    }

    public function actionCreate() {

        if(empty(Yii::$app->request->post('settings', null))){
            return ['status'=>false, 
                'errors'=>'Empty request'
            ];
        }

        $settings_model = new UserSettingsTypes;
        $settings = Yii::$app->request->post('settings');
        $errors = [];

        foreach($settings as $user_setting_id => $setting) {

            $settings_model->setting_type_id = $user_setting_id;
            if(!$settings_model->validate(['user_type_id'])){
                $errors[] = ['user_setting_id'=>$user_setting_id, 'errors'=>$settings_model->getErrors()];
            }

            $user_setting_model = $this->modelProcess($user_setting_id);
            if($user_setting_model['result']) {
            	$user_setting_model = $user_setting_model['model'];
            } else {
            	$errors[] = $user_setting_model['errors'];
            	continue;
            }
            $settingType = $user_setting_model->settingType->settingType;
            $type = $settingType->setting_type;

            $settingType->$type = $setting;
            
            if($settingType->setting_type_id==1) {
            	continue;
            }

            if(!$settingType->validate([$type])) {
                $errors[] = ['user_setting_id'=>$user_setting_id, 'errors'=>$settingType->getErrors()[$type]];
            } else {
                if(empty($setting)) {
                    $setting = "";
                }
                $user_setting_model->value = serialize($setting);
                $user_setting_model->save();
            }

        }
        if(empty($errors)) {
            return ['status'=>true];
        } else {
            return ['status'=>true, 'errors'=>$errors];
        }

    }

/*    public function actionIndex(){
        return Yii::$app->user->identity;
    }*/



}