<?php

/*
	REST Контроллер для социальных сетей
*/

namespace app\controllers;


use app\components\RestController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpBasicAuth;




class ServiceController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        return $behaviors;
    }

/*    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        //$behaviors['authenticator']['only'] = ['update'];
        return $behaviors;
    }*/

    public function actionTest(){
        return \Yii::$app->user->identity;
    }

    public $modelClass = 'app\models\Services';
}
