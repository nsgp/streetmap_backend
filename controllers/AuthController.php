<?php
/**
 * Created by PhpStorm.
 * User: sega
 * Date: 23.03.16
 * Time: 23:26
 */

namespace app\controllers;


use app\components\RestController;

use app\models\authorization\RegistrationForm;
use app\models\authorization\LoginForm;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\AccessControl;
use app\models\mongo\UserImages;
use Yii;

class AuthController extends RestController
{
    public $modelClass = 'app\models\Users';

    public function behaviors()
    {

        $behaviors = parent::behaviors();
        $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'registration'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                        'matchCallback'=>function() {
                            throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                        }
                    ],
                ],
            ];

        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'login' => ['POST'],
            'authorization' => ['POST'],
        ];
    }

    public function actionRegistration(){
        $model = new RegistrationForm();
        if ($model->load(Yii::$app->request->post())) {
            return $model->signup();
        } else {
            return ['status'=>false, 'error'=>['empty request']];
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            return $model->login();
        } else {
            return ['status'=>false, 'error'=>['empty request']];
        }
    }
}