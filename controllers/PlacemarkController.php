<?php
/**
 * Created by PhpStorm.
 * User: sega
 * Date: 23.03.16
 * Time: 23:26
 */

namespace app\controllers;


use app\components\RestController;

use app\models\authorization\RegistrationForm;
use app\models\authorization\LoginForm;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\AccessControl;
use app\models\mongo\UserImages;
use app\models\mongo\Placemarks;

use yii\mongodb\Query;
use yii\helpers\Url;
use Yii;

class PlacemarkController extends RestController
{
    public $modelClass = 'app\models\mongo\Placemarks';

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['authenticator']['only'][] = 'create';
        $behaviors['authenticator']['only'][] = 'image';

        $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'verbs' => ['GET'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'image'],
                        'verbs' => ['POST'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'matchCallback'=>function() {
                            throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                        }
                    ],
                ],
            ];

        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'image' => ['POST'],
            'create' => ['POST'],
            'all' => ['GET']
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['create']);
        unset($actions['view']);
        return $actions;
    }

		public $base_url = "http://catalog.api.2gis.ru/geo/search";
		public $api_key = 'ruczoy1743';

    public function actionView( $id ) {

        return Placemarks::findOne($id);
        //$data = Placemarks::find()->where(['_id'=>$id])->one();
        //if(empty($data)){
        //    return ['status'=>false, 'errors'=>'not found', 'data'=>[]];
        //}
        //
        //return ['status'=>true, 'errors'=>[], 'data'=>$data];

        #$query = new Query;
        #$query->from('points_' . $city_id)->select(['coordinates']);
        #return $query->all(); 
    }


    public function actionIndex( $city = false ) {


        if($city==false) {
            return Placemarks::find()->all();
        } else {
            return Placemarks::find()->where(['city_id'=>intval($city)])->all();
        }

/*        $query = new Query;
        if($city_id == false) {

        }
        $query->from('points_' . $city_id)->select(['coordinates']);
        return $query->all(); */
    }

    public function actionImage(){
        
        return ['status'=>false, 'errors'=>'method in dev proccess'];
        
        $point_id = Yii::$app->request->post('point_id'); 

        if(empty($point_id)){
            return ['status'=>false, 'errors'=>['point_id not send']];
        }

        $images = Yii::$app->request->post('images'); 

        if(!empty($images)) {
            $exist_images = [];

            if(is_array($images)){
                $point = Placemarks::find($point_id)->one();
                foreach($images as $image_code){
                    $image = UserImages::find()->select(['_id', 'photo_id'])->where(['photo_id'=>$image_code])->one(); 
                    if(!empty($image)){
                        $exist_images[] = $image_code;
                    }
                }
            }

            if(empty($exist_images)){
                return ['status'=>false, 'errors'=>['images not found']];    
            }

            if(!empty($point->images)){
                $point->images = array_unique(array_merge($point->images, $exist_images));    
            } else {
                $point->images = array_unique($exist_images);          
            }

            if($point->save()){
                return ['status'=>true];         
            }
        } else {
            return ['status'=>false, 'errors'=>['images not found']];
        }
    }

    public function actionCreate()
    {
        $lat = Yii::$app->request->post('lat');
        $lng = Yii::$app->request->post('lng');
        $user_title = Yii::$app->request->post('user_title');
        $description = Yii::$app->request->post('description');

        $painted_over = Yii::$app->request->post('painted_over', false);
        $free_watch = Yii::$app->request->post('free_watch', false);
        $point_type = Yii::$app->request->post('point_type', 0);

        $images = Yii::$app->request->post('images', false);


        if(!empty($lat) && !empty($lng)){
            $geoCodeUrl = $this->base_url . Url::toRoute(['/', 
                'key' => $this->api_key,
                'version' => 1.3,
                'q'=>trim($lng) . ',' . trim($lat)
            ]);

            $ch    = curl_init($geoCodeUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $contents    = json_decode(curl_exec($ch));

            $pointInfo = [];
            $pointInfo['coordinates'] = ['lat'=>$lat, 'lng'=>$lng];
            $pointInfo['user_id'] = Yii::$app->user->identity->user_id;;

            if(!isset($contents->result[0])){
                $pointInfo['city_id'] = false;
                $pointInfo['centroid'] = false;  
            } else {
                $geoData = $contents->result[0];
                $pointInfo['city_id'] = $geoData->project_id;
                $pointInfo['centroid'] = $geoData->centroid;
                $pointInfo['attributes'] = [
                    'small_description'=>$geoData->name,
                ];  
            }


            $placemark = new Placemarks;
            $placemark->description = !empty($description) ? strip_tags(trim($description)) : '';
            $placemark->user_title = strip_tags(trim($user_title));
            
            $placemark->painted_over = $painted_over;
            $placemark->free_watch = $free_watch;
            $placemark->point_type = $point_type;

            if(!$placemark->validate()){
                return ['status'=>false, 'errors'=>$placemark->getErrors()];    
            }
            
            $pointInfo['status'] = true;
            $pointInfo['attributes']['description'] = $placemark->description;  
            $pointInfo['attributes']['user_title'] = $placemark->user_title;  

            $pointInfo['attributes']['painted_over'] = $placemark->painted_over;  
            $pointInfo['attributes']['free_watch'] = $placemark->free_watch;  
            $pointInfo['attributes']['point_type'] = $placemark->point_type;  
            

            if($images!=false && is_array($images)) {
                $pointInfo['images'] = $images;
            }

            $collection = Yii::$app->mongodb->getCollection('points_1');
            if(!empty($collection->insert($pointInfo))){
                return $pointInfo;
            }

        } else {
            return ['status'=>false, 'errors'=>['Необходимо выбрать точку на карте']];
        }
    }
}