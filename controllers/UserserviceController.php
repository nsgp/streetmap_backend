<?php

/*
	REST Контроллер для социальных сетей
*/

namespace app\controllers;

use app\models\UserServices;
use app\models\Users;
use app\components\RestController;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;
use Yii;

use TwitterOAuth\Auth\ApplicationOnlyAuth;
use TwitterOAuth\Serializer\ArraySerializer;


/**
 * Авторизация через социальные сети
 */
class UserserviceController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => HttpBearerAuth::className(),
        //];
        return $behaviors;
    }

    public $modelClass = 'app\models\UserServices';

    /* Авторизация или регистрация с помощью ВК */

    public  function actionVk($code) {
        $social_id = 1;
        $content = $this->getToken($code, $social_id);

        if(isset($content->error)){
            return $content;
        } else {
            $user_id = $content->user_id;
            $access_token = $content->access_token;
            $user_service_db = UserServices::find()->where(['user_service_id'=>$content->user_id])->one();
            if(empty($user_service_db)){
                $user_info = 'https://api.vk.com/method/users.get?user_id='. $user_id .'&fields=domain&v=5.50';
                $user_info = json_decode(file_get_contents($user_info));
                $user_name = $user_info->response[0]->first_name .' '. $user_info->response[0]->last_name;
                return $this->newUserservice($user_name, $access_token, $user_id, $social_id);
            } else {
                return $this->refreshToken($user_service_db, $access_token);
            }
        }
    }

    public function actionFb( $code ) {
        $social_id = 2;
        $content = $this->getToken($code, $social_id);


        if(isset($content->error)){
            return $content;
        } else {
            $access_token = (string)$content->access_token;
            $base_url = 'https://graph.facebook.com/me?fields=id,name&access_token='.$access_token;
            $ch    = curl_init($base_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $contents    = json_decode(curl_exec($ch));
            $user_id = (string)$contents->id;
            $user_name = (string)$contents->name;

            $user_service_db = UserServices::find()->where(['user_service_id'=>$user_id])->one();
            if(empty($user_service_db)){
                return $this->newUserservice($user_name, $access_token, $user_id, $social_id);
            } else {
                return $this->refreshToken($user_service_db, $access_token);
            }
        }
    }

    public function actionGoogle() {

    }

    public function actionTw(){
        $credentials = array(
            'consumer_key' => 'HIEflr0st7FCFzkE45llZH5LA',
            'consumer_secret' => 'z1n4BiH5wu2TJ2FhPxo53FQ15y8h5xamhDVl144jiFymJcLnaG',
        );

        $serializer = new ArraySerializer();
        $auth = new ApplicationOnlyAuth($credentials, $serializer);
        $bearerToken = $auth->getBearerToken();
        echo 'Bearer Token: ' . $bearerToken . '<hr />';
        $params = array(
            'screen_name' => 'ricard0per',
            'count' => 3,
            'exclude_replies' => true
        );


        $response = $auth->get('statuses/user_timeline', $params);
        echo '<pre>'; print_r($auth->getHeaders()); echo '</pre>';
        echo '<pre>'; print_r($response); echo '</pre><hr />';
        /**
         * If you need to Invalidate a Bearer Token you can invalidate it like this:
         */
        $status = $auth->invalidateBearerToken();
        if ($status === true) {
            echo 'Bearer Token invalidated';
        } else {
            echo 'Error invalidating Bearer Token';
        }
    }

    public function refreshToken($user_service_db, $access_token) {
        $user_service_db->user->auth_key = $access_token;
        if($user_service_db->user->save()){
            return [
                'status'=>true,
                'token'=>$access_token
            ];
        } else {
            return [
                'status'=>false,
                'errors'=>$user_service_db->user->getErrors()
            ];
        }
    }

    /* Получаем обрабатываем пользователя */
    private function newUserservice( $user_name, $access_token, $user_id, $social_id ) {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $user_service_status = false;
        $user_save_status = false;
        $user = new Users;
        try {
            $user_save_status = $user->newUserFromService($user_name, $access_token, 1 );

            if($user_save_status!=false) {
                $user_service = new UserServices;
                $user_service_status = $user_service->addUserService($social_id, (string)$user_id, $user_save_status);
            }
        }
        catch (Exception $e) {

            $transaction->rollBack();
        }

        if($user_service_status && $user_save_status) {
            $transaction->commit();
            return [
                'status'=>true,
                'token'=>$access_token
            ];
        } else {
            $transaction->rollBack();
            return [
                'status'=>false,
                'errors'=>[0=>'unknown error']
            ];
        }
    }

        /* Получаем токен от сервера */
    private function getToken( $code, $social_id ) {

        $client_id = Yii::$app->params['social_config'][$social_id]['client_id'];
        $client_secret = Yii::$app->params['social_config'][$social_id]['client_secret'];
        $redirect_uri = Yii::$app->params['social_config'][$social_id]['accept_redirect_url'];

        $url = Yii::$app->params['social_config'][$social_id]['auth_server'].'?client_id='.$client_id.'&client_secret='.$client_secret.'&redirect_uri='.$redirect_uri.'&code='.$code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $content=curl_exec($ch);
        curl_close($ch);
        $content = json_decode($content);
        return $content;
    }
}