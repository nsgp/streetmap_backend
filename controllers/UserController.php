<?php
/**
 * Created by PhpStorm.
 * User: sega
 * Date: 23.03.16
 * Time: 23:26
 */

namespace app\controllers;


use app\components\RestController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use app\models\mongo\UserImages;
use Yii;

class UserController extends RestController
{
    public $modelClass = 'app\models\Users';

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex(){
        return Yii::$app->user->identity;
    }

    protected function verbs()
    {
        return [
            'index' => ['GET'],
        ];
    }

}